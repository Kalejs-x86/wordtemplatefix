﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

using DocumentFormat.OpenXml.Packaging;

class Logger
{
    private StreamWriter SWriter;
    private readonly string FILE_NAME = @"TemplateFix_Log.txt";

    public bool Initialize(string Directory, out string Error){
        try
        {
            SWriter = File.CreateText(Directory + "\\" + FILE_NAME);
            Error = "";
            return true;
        }
        catch (UnauthorizedAccessException e)
        {
            Error = "Permission denied";
        }
        catch (PathTooLongException)
        {
            Error = "The specified path is too long";
        }
        return false;
    }

    public void Print(string Text)
    {
        Console.WriteLine(Text);
        SWriter.WriteLine(Text);
    }

    public void PrintToFile(string Text)
    {
        SWriter.WriteLine(Text);
    }

    public void Terminate()
    {
        SWriter.Close();
        Console.WriteLine("Log file saved in target directory as " + FILE_NAME);
    }
}

namespace WordTemplateFix
{
    class Program
    {
        // Atļautie paplašinājumi
        private static readonly string[] VALID_EXTENSIONS = { ".docx", ".doc" };
        // Noklusētais template
        private static readonly string[] IGNORE_TEMPLATES = { "Normal", "Normal.dotm" };

        // Noņem visas ārējās atsauces, kuras ir piesaistītas šim dokumentam (var būt vairākas)
        static void FixTemplate(string FilePath)
        {
            using (WordprocessingDocument WordDoc = WordprocessingDocument.Open(FilePath, true))
            {
                var RelationshipsEnum = WordDoc.MainDocumentPart.DocumentSettingsPart.ExternalRelationships;

                foreach (var ExtRel in RelationshipsEnum.ToList())
                {
                    WordDoc.MainDocumentPart.DocumentSettingsPart.DeleteExternalRelationship(ExtRel);
                }
                WordDoc.MainDocumentPart.Document.Save();
            }
        }

        static List<string> GetExternalRelationships(string FilePath)
        {
            List<string> Relationships = new List<string>();
            using (WordprocessingDocument WordDoc = WordprocessingDocument.Open(FilePath, false))
            {
                var RelationshipsEnum = WordDoc.MainDocumentPart.DocumentSettingsPart.ExternalRelationships;
                foreach (var ExtRel in RelationshipsEnum)
                {
                    Relationships.Add(ExtRel.Uri.ToString());
                }
            }
            return Relationships;
        }

        static string GetTemplateName(string FilePath)
        {
            using (WordprocessingDocument WordDoc = WordprocessingDocument.Open(FilePath, false))
            {
                var FileProp = WordDoc.ExtendedFilePropertiesPart.Properties;
                return FileProp.Template.Text;
            }
        }

        static void PrintUsage()
        {
            Console.WriteLine("Identifies which Microsoft Word documents in <TargetDirectory> use external references (networked templates) and resets them to the default template (optional)\n");
            Console.WriteLine("Program usage:");
            Console.WriteLine("WordTemplateFix TargetDirectory");
        }

        static void ShortenString(ref string Text, int MaxChars)
        {
            if (Text.Length > MaxChars)
            {
                string szBuffer = Text.Substring(0, MaxChars - 3);
                szBuffer += "...";
                Text = szBuffer;
            }
        }

        static bool YesNoPrompt(string Message)
        {
            while (true)
            {
                Console.WriteLine(Message);
                string szInput = Console.ReadLine();
                szInput = szInput.ToLower();

                if (szInput == "y") return true;
                else if (szInput == "n") return false;
            }
        }

        static void Main(string[] args)
        {
            // Nepareizi parametri
            if (args.Length != 1 || args[0].Contains('?')){
                PrintUsage(); return; // Izvadam pamācību
            }

            string szTargetDir = Path.GetFullPath(args[0]);
            if (!Directory.Exists(szTargetDir)){
                Console.WriteLine("Target directory \"" + szTargetDir + "\" does not exist"); return;
            }

            Logger Log = new Logger();
            bool bLogFileOK = Log.Initialize(szTargetDir, out string szLogInitError);
            if (!bLogFileOK)
            {
                Console.WriteLine("Failed to create log file in the specified directory; Error:");
                Console.WriteLine(szLogInitError);
                return;
            }

            // Atrasto dokumentu skaits
            int iNumWordDocs = 0;
            // Dokumenti, kuriem ir nepieciešamas izmaiņas
            List<string> BadDocs = new List<string>();

            Log.Print("Looking for Word Documents with valid XML data in \n" + szTargetDir);
            Log.Print("");
            // Iziet cauri visiem failiem norādītajā direktorijā
            foreach (string szFilePath in Directory.EnumerateFiles(szTargetDir))
            {
                string szExtension = Path.GetExtension(szFilePath).ToLower();
                // Vai faila paplašinājums ir .doc vai .docx?
                if (VALID_EXTENSIONS.Contains<string>(szExtension))
                {
                    string szFileName = Path.GetFileName(szFilePath);
                    // Mēģinam ielasīt informāciju par failu
                    try
                    {
                        var ExtRels = GetExternalRelationships(szFilePath);
                        if (ExtRels.Count != 0)
                            BadDocs.Add(szFilePath);
                        iNumWordDocs++;
                    }
                    catch (UnauthorizedAccessException e){
                        Log.Print("Failed to read file \"" + szFileName + "\": Permission denied");}
                    catch (OpenXmlPackageException e){
                        Log.Print("Failed to read relationship information from \"" + szFileName + "\": Document not valid for Open XML WordprocessingDocument");}
                    catch (FileFormatException e){
                        Log.Print("Failed to read relationship information from \"" + szFileName + "\": File contains corrupt data");
                    }
                }
            }

            Log.Print("");
            Log.Print(BadDocs.Count + " out of " + iNumWordDocs + " valid Word documents in this directory use an external template");
            // Izvada informāciju par atrastiem failiem
            foreach (string szFilePath in BadDocs)
            {
                string szFileName = Path.GetFileName(szFilePath);
                Log.Print("* \"" + szFileName + "\" [" + GetTemplateName(szFilePath) + "]"); // * "Piemers.docx" [Veidne.dotm] 

                foreach (string ExtRel in GetExternalRelationships(szFilePath))
                {
                    string URI = ExtRel;
                    Log.PrintToFile("\t- " + URI);

                    ShortenString(ref URI, 36);
                    Console.WriteLine("\t- " + URI);
                }
            }

            Console.WriteLine("\n");

            if (BadDocs.Count != 0)
            {
                bool bAnswer = YesNoPrompt("Delete external references? (will OVERWRITE the files) [y/n]");
                if (bAnswer)
                {
                    foreach (string szFilePath in BadDocs)
                    {
                        // Veic izmaiņas
                        FixTemplate(szFilePath);
                    }
                    Log.Print("Removed external references from " + BadDocs.Count + " documents\n");
                }
                else
                    Log.Print("No changes were made\n");
            }

            Log.Print("Terminating program");
            Log.Terminate();
            Console.WriteLine("\nPress any key to exit...");
            Console.ReadKey();
        }
    }
}
