# WordTemplateFix
A simple commandline tool for removing references to networked templates in Microsoft Word documents.

## Description
This commandline tool allows you to identify which Microsoft Word documents use networked templates and remove them en masse if necessary.

It is useful if a collection of Word documents, which were created based on a template file located on a network, cannot be opened anymore due to the template file having been renamed or removed. In that case, upon opening said document, Microsoft Word might freeze or try to load the missing file indefinitely, not allowing the user to open the document via Word and remove the template dependency from the document.

While this result can also be achieved by manually editing the appropriate XML files contained in said documents, this is not an option if there are hundreds of documents like this. That's why this software was created to automate this process.

## Built With
* [OpenXML SDK v2.5.5631.0](https://www.nuget.org/packages/DocumentFormat.OpenXml/) - For editing Microsoft Document properties

## License
This project is licensed under the MIT License - see the LICENSE.txt file for details